package http

import functional.Either
import functional.error
import functional.value
import okhttp3.*
import java.lang.Exception
import java.net.SocketTimeoutException

sealed class Http(val url: HttpUrl, val headers: Headers) {
    class Post   (url: HttpUrl, headers: Headers = Headers.of(), val body: RequestBody? = null) : Http(url, headers)
    class Put    (url: HttpUrl, headers: Headers = Headers.of(), val body: RequestBody? = null) : Http(url, headers)
    class Get    (url: HttpUrl, headers: Headers = Headers.of())                                : Http(url, headers)
    class Delete (url: HttpUrl, headers: Headers = Headers.of(), val body: RequestBody? = null) : Http(url, headers)
}

fun formBody(vararg values: Pair<String, String>) = FormBody.Builder().apply { values.forEach { add(it.first, it.second) } }.build()

fun performHttp(client: OkHttpClient, request: Http): Either<HttpError, Response> = try {
    val result = client.newCall(Request.Builder()
        .url(request.url)
        .headers(request.headers)
        .apply { when(request) {
            is Http.Post   -> post(request.body)
            is Http.Put    -> put(request.body)
            is Http.Get    -> get()
            is Http.Delete -> delete(request.body)
        } }
        .build()).execute()

    when(result.isSuccessful) {
        true  -> value(result)
        false -> error(HttpError.BadResponse(result.code(), result.headers(), try { result.body().string() } catch (e: Exception) { "" }))
    }
} catch(e: SocketTimeoutException) {
    error(HttpError.Timeout("${e.javaClass}: ${e.message ?: "No more information"}"))
} catch(e: Exception) {
    error(HttpError.ConnectionProblem("${e.javaClass}: ${e.message ?: "No more information"}"))
}

sealed class HttpError {
    data class ConnectionProblem (val message: String)                                   : HttpError()
    data class Timeout           (val message: String)                                   : HttpError()
    data class BadResponse       (val code: Int, val headers: Headers, val body: String) : HttpError()
}


