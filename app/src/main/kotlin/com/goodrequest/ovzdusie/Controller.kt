package com.goodrequest.ovzdusie

//import org.junit.experimental.results.ResultMatchers.isSuccessful
import android.app.Activity
import android.util.Log
import com.google.gson.Gson
import functional.Either
import http.Http
import http.HttpError
import http.performHttp
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.Response
import rx.Observable
import rx.schedulers.Schedulers
import rx.subjects.BehaviorSubject


class Controller(val activity: Activity) {

    //	http://www.shmu.sk/dynamic/oko/oko_data.php?id=99252&mid=39001&ac=16515616
    val client = OkHttpClient.Builder().build()
    val gson = Gson()

    fun getData(id: String, mid: String, ac: String) {
        val request = Http.Get(HttpUrl.parse("http://www.shmu.sk/dynamic/oko/oko_data.php?id=$id&mid=$mid&ac=$ac"))

        val sub: Observable<Either<HttpError, Response>> = Observable.create({
            it.onNext(performHttp(client, request))
        })

        sub.subscribeOn(Schedulers.io()).subscribe({
            when (it) {
                is Either.Value -> (activity.application as App).cache.onNext(responseToCache(it.value.body().string()))  
                is Either.Error -> Log.d("testik", it.value.toString())
            }
        })
    }

    /**
     * json example : [[1486623600000,63.2],[1486627200000,null],[1486630800000,34.7]]
     * first I have to get all date as array of arrays of doubles
     * after that I convert it to my structure 
     */
    private fun responseToCache(jsonString:String) =  Habla(polutionArrayToList(jsonStringToPolutionArray(jsonString)))

    private fun jsonStringToPolutionArray(string: String): Array<Array<Double?>> = gson.fromJson<Array<Array<Double?>>>(string, Array<Array<Double?>>::class.java)

    private fun polutionArrayToList(polutionArray: Array<Array<Double?>>) = PolutionList(polutionArray.filter({ !it.contains(null) })
        .flatMap { poleDouble ->
            (poleDouble.map { Polution(poleDouble[0]?.toLong()!!, poleDouble[1]!!) })
        })

}


//val cache = (activity.application as App).cache
