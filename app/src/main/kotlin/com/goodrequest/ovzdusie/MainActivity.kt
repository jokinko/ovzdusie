package com.goodrequest.ovzdusie

import android.app.Activity
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.github.mikephil.charting.data.DataSet
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.android.synthetic.main.activity_main.*
import rx.android.schedulers.AndroidSchedulers
import rx.subscriptions.CompositeSubscription

class MainActivity : AppCompatActivity() {

    val presenter = Controller(this)
    val Activity.app: App get() = application as App

    val subscription = CompositeSubscription()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //   	http://www.shmu.sk/dynamic/oko/oko_data.php?id=99252&mid=39001&ac=16515616
        etId.setText("99252")
        etMid.setText("39001")
        etAc.setText("16515616")

        btnGetData.setOnClickListener {
            presenter.getData(etId.text.toString(), etMid.text.toString(), etAc.text.toString())
        }

        app.cache
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                chart.data = toChartData(it.polutionList,"Time")
                chart.invalidate()
            }).apply(subscription::add)
    }

    override fun onDestroy() {
        subscription.takeIf { !it.isUnsubscribed }?.apply(CompositeSubscription::unsubscribe)
        super.onDestroy()
    }

    private fun toEntries(polutionList: PolutionList) = polutionList.polution.map { Entry( it.timestamp.toFloat(), it.timestamp.toFloat()) }.toList()
    private fun toChartData(polutionList: PolutionList, labelName: String) = LineData(LineDataSet( toEntries(polutionList), labelName))
}

