package functional


sealed class Either<out Error, out Value> {
    data class Error<out A> (val value: A): Either<A, Nothing>()
    data class Value<out A> (val value: A): Either<Nothing, A>()
}

fun <A> value(value: A): Either<Nothing, A> = Either.Value(value)
fun <A> error(value: A): Either<A, Nothing> = Either.Error(value)

fun <A> either(action: () -> A): Either<Exception, A> = try { value(action()) } catch (e: Exception) { error(e) }

inline infix fun <E, V, V2> Either<E, V>.flatMap(func: (V) -> Either<E, V2>): Either<E, V2> = when(this) {
    is Either.Error -> this
    is Either.Value -> func(value)
}

inline infix fun <E, V, E2> Either<E, V>.mapError(func: (E) -> E2): Either<E2, V> = when(this) {
    is Either.Error -> Either.Error(func(value))
    is Either.Value -> this
}

inline fun <E, V, A> Either<E, V>.fold(error: (E) -> A, value: (V) -> A): A = when(this) {
    is Either.Error -> error(this.value)
    is Either.Value -> value(this.value)
}